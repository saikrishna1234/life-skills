## Learning Process

**1. What is the Feynman Technique? Paraphrase the video in your own words.**

- If we can't explain it simply, means we don't understand.
- If we want a better understanding of a topic. We have to explain it.
- Explaining a concept helps us to understand better, whether it is history, math, or web development.
- While trying to understand a concept. We have to work on the concept by trying out various examples.

**2. What are the different ways to implement this technique in your learning process?**

- Instantly pick out the areas where we are shaky and need to-do extra work.
- we have to adapt the process of explaining as simply as possible even a fresher can able to understand, Which helps in our learning process.
- We have to question ourselves about how can we frame it simply and explain it. It gives us a better understanding.

**3. Paraphrase the video in detail in your own words.(talk by Barbara Oakley)**

- The brain is too complex to mold but we can do it in 2 modes one is focus mode and the other is diffuse mode.
- Focus mode: Turn your attention to something.
- Diffuse mode: Keeping the brain in diffuse mode gives new ideas.
- When learning new things we have to back and forth between these modes.
- If we get stuck somewhere we need to turn our attention away from the problem and allow the diffuse mode resting stage.

**4. What are some of the steps that you can take to improve your learning process?**

- Doing work with utmost concentration for at least 25 minutes.
- Relaxation is also a process of learning.
- Exercise within a matter of few days increases the ability to learn and remember.
- Testing ourselves all the time by taking little mini-tests.
- Doing flashcards helps us to remember things quickly.
- Learning "How to learn" is the most powerful to you can ever grasp.

**5.Your key takeaways from the video? Paraphrase your understanding.(Learning anything in 20hours)**

- How to deconstruct the skill into smaller parts.
- Explained the sources about the way how he gathered information.
- Finally concluded that 20 hours is enough to learn something new.
- He wondered about how he invented something to learn quickly.
- Explained techniques with that we can learn any new thing in just 20 hours.

- **6. What are some of the steps that you can while approaching a new topic?**

- Deconstructing the skill means breaking the content into as smaller parts as possible, learning important things first then trying to practice those things.
- Learning enough to self-correct means first learning some parts and then practicing solving new problems on that know solutions on resources, Notifying your mistakes from that, and don't think to complete all the content at once and practice.
- By doing the trial and error method helps us to learn something new.
- Remove practice barriers that mean unnecessary like tv internet and mobile.
- By pre-committing to practicing whatever itis that you want to do for at least 20 hours to overcome initial fear.
