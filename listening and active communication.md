## Listening And Active Communication

<br>

**1. What are the steps/strategies to do Active Listening?**

The act of fully hearing and comprehending the meaning of what someone else is saying is called Active Listening.

Steps/ Strategies to do Active Listening.

- Avoid destructing by our own thoughts.
- Focus on the speaker and the topic.
- Try not to interupt the other persons while talking. Let them finish and then respond.
- Show that your listening with body language.
- Repeat the question to make sure that you both are in the same phase.
- Keep a note for storing information, If it is appropriate.

**2. According to Fisher's model, what are the key points of Reflective Listening?**

- Keep the speaker tone and nonverbal communication in mind to create reflection.
- Summarise speaker in your own words.
- Focus on conversation and eleminating distraction.

**3. What are the obstacles in your listening process?**

- Undesired thoughts and imagination.
- Being not able to show intrest in the topic.
- Prejudice about the speaker or topic.
- Speaker's speaking speed is faster than one can understand.
- Lack of vocabulary.
- Not able to reflect speaker's thoughts.

**4. What can you do to improve your listening?**

- Take notes while important conversation.
- Show physical gestures that I am intrested in listening.
- Reflect what speaker is speaking.
- Let speaker finish before asking my doubts.
- Focus on speaker rather than making imaginations.

**5. When do you switch to Passive communication style in your day to day life?**

- In front of elders.
- In a situation where we are not ready to debate with him.
- Sometimes with friends.
- With higher officals like mentors/teachers.

**6. When do you switch into Aggressive communication styles in your day to day life?**

- If get hurted by someone's thoughts or actions.
- When no one is ready to listen my point.
- When mood is not good.

**7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?**

- During conversation with friends.
- In conversation with opposite gender.
- Silent treatment when I am not intrested in someone's conversation.
- When I don't want to hurt someone and still want to put my point.

**8. How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?**

- Respecting other's feelings as equally as ours.
- Putting needs and feelings of all at the same stage.
- Giving importance to my own feeling too.
- Showing discomfort with words.
- Showing discomfort with body posture and other non verbal gestures.
