## Grit and Growth Mindset

**1. Paraphrase (summarize) the video in a few lines. Use your own words.**

- A significant predictor of success is “grit” or “passion and perseverance for very long-term goals.
- Grit is living life like it's a marathon, not a sprint
  One characteristic emerged as a significant predictor of success.” This characteristic was not “social intelligence, good looks, physical health, or IQ – it was grit”.
- Angela Lee Duckworth states that a growth mindset is the best idea she has heard about building grit.
- We need to take our most promising ideas, and strongest intuitions, test, and measure them. We have to be willing to fail, to be wrong, and start over again with lessons learned.”
- To learn anything you need the courage to be on that path which is not easy to pursue, and for that courage, you need to be dedicated to a growth mindset.

**2. What are your key takeaways from the video to take action on?**

- We have to be willing to fail, to be wrong, and to start over again with lessons learned.
- We can grow our mindset to build or learn something that is not fixed we can learn without borders.
- Efforts & consistency will change mindset.
- Don't believe in failure which is not permanent.
- Grit understanding the personality of a person what he/she is going to do what is the ability of the personal motivation regarding the person's growth and willingness to have a strong mindset.

## Introduction to Growth Mindset

**3. Paraphrase (summarize) the video in a few lines in your own words.**

- The video mainly shows the difference between a Fixed Mindset and a Growth mindset.
- People with Growth Mindset believe that skills and intelligence are grown and developed.
- The belief that we are in control of our skills. That they are things that can be developed and improved is called Growth Mindset.
- The belief that our skills are set, that what we have, and that we cannot change them is called a Fixed Mindset.
- Someone with a growth mindset views intelligence, abilities, and talents as learnable and capable of improvement through effort. On the other hand, someone with a fixed mindset views those same traits as inherently stable and unchangeable over time.
- Building a growth mindset can encourage more learning actions, which helps people learn, grow, and get better.

**4. What are your key takeaways from the video to take action on?**

- Always try to improve by taking feedback.
- Don't let the failure think you can't improve.
- Be ready to try to find and solve new problems.

## Understanding Internal Locus of Control

**5. What is the Internal Locus of Control? What is the key point in the video?**

- Locus of control which is essentially the degree to which you believe you have control over your life.
- Having an internal locus of control is the key to staying motivated.
- Internal locus of control is the key to staying motivated.
- Build up the belief that you are in control of your destiny, that you have an internal locus of control, and you will never have issues with motivation in your life.
- The three best ways to followed to adopt an Internal Locus of Control and to start being motivated are:

1. Solving Problems in one’s Life
2. Taking Time to Appreciate one’s actions
3. Noticing improvements and efforts taken by an individual

- It is one of the key factors and it will make us better not to be demotivated.

## How to build a Growth Mindset

**6. Paraphrase (summarize) the video in a few lines in your own words.**

- Growth mindset sees opportunities instead of obstacles, choosing to challenge themselves to learn more rather than sticking in their comfort zone.
- Instead of saying ‘I’ll get better eventually’, be more specific. Say I’m going to get better and try to be more active.
- A growth mindset allows you to explore more, take more risks, try new things, and grow more into what you’re capable of.
- To achieve a growth mindset, causing a development plan is crucial.
  
**7. What are your key takeaways from the video to take action on?**
- Don't be afraid of challenges because life has surprises for you in every way. Some are good, and some are bad. But be prepared to take on challenges.
- Honor the struggle and Create your curriculum for skill development.
- A growth mindset means that you thrive on challenges, and don’t see failure as a way to describe yourself.
- Taking time to appereciate one’s actions.
- Believe in your ability to figure things out.
- Question your negative assumptions.

## Mindset - A MountBlue Warrior Reference Manual

**8. What are one or more points that you want to take action on from the manual? (Maximum 3)**

- More efforts lead to better understanding.
- Will stay relaxed and focused no matter what happens.
- Will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.
