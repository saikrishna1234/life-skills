# Energy Management

**1. What are the activities you do that make you relax - Calm quadrant?**

- Taking a bath: Taking a warm bath can help to relax muscles and calm the mind.
- Riding a bike: Riding can be a great way to escape from the stresses of everyday life and relax.
- Meditation or deep breathing exercises: This can help me to focus on my breath and clear my mind.
- Going for a walk: A peaceful walk in nature can be a great way to clear your mind.
- Spending time with family members helps me to feel calm and relaxed.

**2. When do you find getting into the Stress quadrant?**

- Adjusting to new environments: Starting an office can be stressful, as freshers have to adjust to a new environment, make new companions, and navigate a new system.
- Time management: Managing time effectively can be a challenge for me, especially when juggling multiple assignments and extracurricular activities.
- Exam season: The pressure of preparing for exams, deadlines, and the fear of failure can all contribute to stress.
- Pressure to succeed: The pressure to succeed professionally can lead to stress and anxiety.

**3. How do you understand if you are in the Excitement quadrant?**

- When looking forward to something with excitement and anticipation, such as a new job, a trip, or a big event.
- Feeling excited and eager to take on new challenges or explore new opportunities is also a sign.
- When feeling happy, optimistic, and confident about the future.
- Heart rate and breathing elevated is a sign of indicating the excitement quadrant.
- When highly focused on the task at hand and fully engaged in what I was doing is a sign of excitement quadrant.

**4. Paraphrase the Sleep is your Superpower video in detail.**

- A person who sleeps 4hr or less in a day has 40% less learning and understanding of the concept as compared to the who sleeps 7hr or more.
- Sleep is an essential activity in life. It is necessary to sleep enough to stay in good health.
- Those who sleep 7hr have high memory power because during sleeping what we have learned will be transferred from short-term memory to permanent memory.
- When we lose 1hr of sleep then the heart attack chances are increases by 24%.
- Studies have shown people with less sleep are prone to have heart attacks, our immune systems are also being affected, and aging is also expected to sleep deprivation. Lack of sleep can result in major health issues.
- By looking at all the above points, I can say sleep is a superpower.

**5. What are some ideas that you can implement to sleep better?**

- Stress and anxiety can interfere with sleep, so I do practice stress-management techniques such as meditation or deep breathing exercises.
- The blue light emitted by electronic devices can disrupt sleep, so turn off your devices at least an hour before bedtime.
- Go to bed and wake up at the same time every day, even on weekends.
- Take a warm bath, read a book, or practice relaxation techniques such as meditation or deep breathing exercises.
- Keeping the bedroom cool, dark, and quiet. Investing in comfortable bedding and pillows.
- Heavy meals can disrupt sleep, so taking a light dinner helps us to get better sleep.

**6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.**

- In the video speaker neuroscientist, Wendy Suzuki talks about the importance of doing exercise.
- What's the most transformative thing that you can do for your brain today? Exercise! speaker said. She gets inspired to go to the gym.
- She said how working out boosts your mood and memory and protects your brain against neurodegenerative diseases like Alzheimer's.
  Some of the brain-changing benefits of exercise are
- Immediate increases focus on what we do.
  The volume of focus/ attention increases.
- Increases neurotransmitters in our brain.
- Feel better and more energetic.
- Improves memory power, reaction time.

**7. What are some steps you can take to exercise more?**

Following steps, that I will take to exercise more
- Walking whenever possible instead of driving.
- Punctuality is the matter for this, So I will allocate time for exercise activity which makes me mentally and physically stronger.
- It only takes some effort and willingness to do so. I can simply add some physical activities like jogging an extra mile, sport jumping, hand movements, and some aerobic exercises to pump your heart up a little.
- Using health apps on a smartphone.
- Tracker app to track the streaks.
