## REST

## What Is REST API?

An API (Application Program Interface) is a set of functions and procedures that enables different programs to communicate with one another.

REST (Representational State Transfer) architecture is a software architecture style for building distributed systems that use web-based communication. It defines the set of rules to be used for creating web services.

<br>
<div>
  <img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1592681809843/mRdDG3kkE.png?auto=compress,format&format=webp"/>
</div>
<br>

HTTP Verbs
There are 4 basic HTTP verbs we use in requests to interact with resources in a REST system:

1. GET - Retrieve a specific resource (by id) or a collection of resources
2. POST - create a new resource
3. PUT - update a specific resource (by id)
4. DELETE - remove a specific resource by id

- Roy Thomas Fielding (born 1965) is an American computer scientist, one of the principal authors of the HTTP specification and the originator of the Representational State Transfer (REST) architectural style.

- In REST architecture, resources are identified by URIs, and clients can use HTTP methods (such as GET, POST, PUT, DELETE) to perform CRUD (Create, Read, Update, Delete) operations on those resources.

- The server responds with representations of the requested resources using standard formats such as JSON or XML.
- This allows for greater scalability and reliability of the system. REST also makes the use of standard protocols and formats, making it easier for clients and servers to communicate.

There are six architectural constraints which makes any web service are listed below:

## Cacheable

A response data should be cacheable to recycle data for similar requests. This reduces client-server interactions, which improves performance and saves time.

## Client-Server Architecture

A principle that states that an API design should isolate client and server components. Any communication or requests must be handled by HTTP requests.

## Code on Demand

Allow servers to deliver executable code to extend the functionality of the client in addition to returning static XML or JSON data.

## Layered System

Having additional layers on the client-server system in which each layer is a component that cannot interact outside the subsequent layer. These layers can offer additional features like security, load balancing and scalability.

## Stateless

All requests from the client must include all the data the server needs to complete the request. Each request are called independently.

## Uniform Interface

A REST API must have a uniform interface which can simplify the system architecture and allow each part to evolve independently.

<br>

## Resources

- https://www.youtube.com/watch?v=qVTAB8Z2VmA
- https://www.geeksforgeeks.org/rest-api-architectural-constraints/
- https://www.redhat.com/en/topics/api/what-is-a-rest-api
