# Focus Management

## What is Deep Work

**What is Deep Work?**
- Deep work is a state of peak concentration that lets us learn hard things and create quality work quickly.
- Deep work is when you can give yourself 100% to the task with complete focus and without distraction. 
- Speaker Cal Newport suggested that to be truly effective, each of us should log out of all communication tools for multiple hours daily to sustain our focus. 
- It is the ability to concentrate on your work. 
Newport defines deep work as a period of distraction-free concentration when your brain works at its maximum potential.

## Summary of Deep Work Book

**Paraphrase all the ideas in the above videos and this one in detail.**

- Cal Newport defines deep work as professional activities performed in a state of distraction-free concentration that pushes your cognitive abilities to the limit. These efforts create new value and improve skills. 
- Schedule distractions and train your brain to take the distraction breaks in preplanned ways.
- The brain advancement we get from deep work allows us to rapidly connects ideas and uncover creative solutions.
- Deep work generates a rhythm of work to do deep work.
Sleep is the price we pay for intense deep work.
- Speaker says that the optimal duration for deep work is 60 to 90min. 90min would be good because during these 90 minutes, sometimes we switch to something else to find or looking something that would not be considered in your deep work.
- He also talks about a deadline. He said that the deadline is a motivational signal. Our mind debate every 3min whether to take a break now or later. 
Deadline gives us the power to defeat our minds to do the work on or before the deadline.
- Because of the deep work, Harry potter has been documenting successfully, Buil gate makes the basic version of Microsoft code.
- When we practice deep work we upgrade our brains and allow specific brain circuits to fire more effortlessly and effectively.  

**How can you implement the principles in your day to day life?**

We can implement this principle in our day-to-day life pretty efficiently by doing daily practice. We need to find our daily productive time to do deep work. We need to increase this deep work time at a slow rate and try to achieve as much as we are capable of.
- Organize the deep work.
- Work on what matters.
- Develop a deep work schedule and routine.
- Meditate.
- Take breaks.
- Concentrate on your energy.
- Schedule Tight Deadlines.
- Define Your personal and professional goals.
- Define your key activities.
- Try It for 30 days.


## Dangers of Social Media

**Your key takeaways from the video**

- These social media wants to addict users with their social sites so that we user can spend more of our time with those sites and by collecting our every moment record they will make money. 
- Social media is not necessarily automation, and we have to use it.
- Social media makes them incapable of concentrating on tasks for a long time. Social media is harmless and is not an essential part of life beyond social media.
- It makes us lose the ability to sustain concentration, Going to become less and less relevant to this economy. 
- There is also psychological harm that the more use of social media, the more likely you are to go to feel lonesome.
- If you want to stop using social media, then in the first two weeks going to be uncomfortable, and you sense a  slight bit anxious, But after things settle down, life after social media can be quite good and positive, and you will be quite productive. 
