## Prevention of Sexual Harassment

**What kinds of behaviour cause sexual harassment?**
While there are some apparent ways that people are molested like unwanted kissing, touching of privates, butt slapping, making sexually explicit comments, uninvited massages, requests for sexual favors, sexually suggestive gestures, ogling, catcalls or cornering someone in a tight space, there are more subtle forms of harassments too. Some of them are listed below:

- Commenting on the attractiveness of others in front of an employee.
- Leaving unwanted gifts of a sexual or romantic nature.
- Making sexual jokes.
- Repeated compliments of an employee's appearance.
- Spreading sexual rumors about a colleague.


**What would you do in case you face or witness any incident or repeated incidents of such behaviour?**

If I would face or witness any sexual harassment incident repeatedly, firstly I would warn the person to don't do this. If the person didn't back off, I will talk with the supervisor.Regardless of the type of harassment you are experiencing, it is important to document everything. Write down details such as:

- The date, time, and location of the harassment, what happened, what was said, and who witnessed the behavior.
- Tell a trusted friend, family member, or co-worker what happened, and write down the details of those conversations. 
- Keep copies or take screenshots of any relevant emails, texts, photos, or social posts.
- Keep records related to your productivity and job performance and, if possible, review your performance report or personal file. This is so you have evidence should your performance ever be disputed.
